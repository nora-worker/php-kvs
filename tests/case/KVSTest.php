<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Module\KVS;

use Nora;

/**
 * KVSModuleのテスト
 *
 */
class KVSTest extends \PHPUnit_Framework_TestCase
{
    public function testModule ( )
    {
        Nora::logging_start()->addLogger([
            'writer' => 'stdout'
        ]);
        Nora::environment_register();

        return Nora::Module('KVS');
    }

    /**
     * @depends testModule
     */
    public function testKVS($m)
    {
        $m->open();
        $m->write(1, 'hajime');
        $this->assertEquals('hajime', $m->read(1));
        sleep(2);
        $m->gc(1);
        $this->assertFalse($m->has(1));
    }
}
