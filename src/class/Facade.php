<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Module\KVS;

use Nora\Core\Module\ModuleIF;
use Nora\Core\Module\Modulable;
use function Nora\__;

/**
 * KVSモジュール
 */
class Facade extends Handler implements ModuleIF
{
    use Modulable;

    private $_engine = false;

    protected function initModuleImpl( )
    {
    }

    public function open($spec = null)
    {
        if ($spec === null)
        {
            $spec = $this->configure( )->read('kvs.spec', 'file:///tmp/kvs');
        }
        $this->setSpec($spec);

        parent::open( );
    }

    public function createHandler($spec)
    {
        $handler = new Handler( );
        $handler->setSpec($spec);
        $handler->setScope($this->newScope());
        return $handler;
    }
}
