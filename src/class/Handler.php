<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Module\KVS;

use function Nora\__;
use Nora\Core\Util\SpecLine;
use Nora\Core\Component\Componentable;

/**
 * KVSモジュール
 */
class Handler 
{
    use Componentable;

    private $_engine = false;
    private $_spec = false;

    protected function initComponentImpl( )
    {
    }

    protected function setSpec($spec)
    {
        $this->_spec = $spec;
    }


    public function open( )
    {
        $spec = SpecLine::create($this->_spec);
        $type = $spec->getType( );

        $class = __namespace__.'\\Engine\\'.ucfirst($type);

        $engine = new $class($spec);
        $engine->setSpec($spec);
        $engine->setScope($this->newScope('engine'));
        $engine->open( );
        $this->_engine = $engine;
    }

    public function write($key, $value)
    {
        return $this->engine()->write($key, $value);
    }

    public function read($key)
    {
        return $this->engine()->read($key);
    }

    public function gc($time)
    {
        return $this->engine()->gc($time);
    }

    public function has($key)
    {
        return $this->engine()->has($key);
    }

    public function delete($key)
    {
        return $this->engine()->delete($key);
    }

    public function engine( )
    {
        if (!($this->_engine instanceof Engine\Base))
        {
            $this->err(__('KVSエンジンはまだオープンされていません'));
        }
        return $this->_engine;
    }
}
