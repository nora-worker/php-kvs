<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Module\KVS\Engine;

use Nora\Core\Component;

/**
 * KVS-Engine
 */
abstract class Base
{
    private $_spec;

    use Component\Componentable;

    protected function initComponentImpl( )
    {
    }

    public function setSpec($spec)
    {
        $this->_spec = $spec;
    }
    public function getSpec()
    {
        return $this->_spec;
    }

    /**
     * エンジンをオープンする
     */
    abstract public function open( );

    abstract public function has($key);

    abstract public function read($key);

    abstract public function delete($key);

    abstract public function write($key, $value);

    abstract public function gc ($time, $dir = null);

}

