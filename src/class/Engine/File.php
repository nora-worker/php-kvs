<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Module\KVS\Engine;

use Nora\Core\Module\Module;

/**
 * KVS-Engine
 */
class File extends Base
{
    const CHUNK_SIZE=8;

    private $_dir;

    public function open( )
    {
        $this->_dir = $this->getSpec()->getPath();
        $this->logDebug([
            'dir' => $this->_dir
        ], 'kvs.open');

        // データ保存用のディレクトリを作成
        if (!is_dir($this->_dir)) {
            mkdir($this->_dir, 0777, true);
            chmod($this->_dir, 0777);
        }
    }

    public function has($key)
    {
        $file = $this->keyToFileName($key);
        return file_exists($file);
    }

    public function read($key)
    {
        if ($this->has($key))
        {
            $file = $this->keyToFileName($key);
            return file_get_contents($file);
        }
        $this->err("$key は存在しません");
    }

    public function delete($key)
    {
        if ($this->has($key))
        {
            $file = $this->keyToFileName($key);
            $this->logDebug([
                'delete-key' => $key,
                'file' => $file
            ]);
            unlink($file);
        }
    }

    public function write($key, $value)
    {
        $file = $this->keyToFileName($key);

        if (!is_dir($d = dirname($file)))
        {
            mkdir($d, 0777, true);
            chmod($d, 0777);
        }

        // 書き込む
        file_put_contents($file, $value);

        $this->logDebug([
            'key' => $key,
            'file' => $file,
        ], 'kvs.write');

        return true;
    }

    public function gc ($time, $dir = null)
    {
        if ($dir === null) $dir = $this->_dir;

        foreach(glob($dir.'/*') as $f)
        {
            if (is_dir($f))
            {
                $this->gc($time, $f);
            }else{
                $target_time = time() - $time;
                if (filemtime($f) < $target_time)
                {
                    unlink($f);
                }
            }
        }
    }

    private function keyToFileName($key)
    {
        $code = md5($key);

        $file = implode(
            '/',
            preg_split(
                "/\r\n/",
                trim(
                    chunk_split($code, self::CHUNK_SIZE)
                )
            )
        );

        $file = $this->_dir.'/'.$file;
        return $file;
    }

    public function close( )
    {
        return true;
    }
}

